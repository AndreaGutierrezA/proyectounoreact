import React from 'react'; // Esta linea no es necesaria en esta nuevas versiones
// importacion de los estilos
import '../hojas-de-estilo/Testimonio.css'

// creacion de un componente
// exportacion nombrada se usa de esta manera -> export function Testimonio

// definir los props -> que son las propiedades que tendran los valores, para ello ir a App.js quien es quien lo crea o renderiza
function Testimonio(props) {
  return (
    <div className='contenedor-testimonio'>
      <img
        className='imagen-testimonio'
        // formas de mezclar javascript
        // insertar uma imagen se usa requiere{(``)}
        src={require(`../imagenes/testimonio-${props.imagen}.png`)}
        alt='Fotografia de Emma' />
      <div className='contenedor-texto-testimonio'>
        <p className='nombre-testimonio'>
          <strong>{props.nombre}</strong> en {props.pais}</p>
        <p className='cargo-testimonio'>{props.cargo} en <strong>{props.empresa}</strong></p>
        <p className='texto-testimonio'>'{props.testimonio}'</p>
      </div>
    </div>
  );
  
  // mostrar el componente para ello se debe renderizar el componte en App.js
}
// aqui se exporta el archivo por medio de exportacion por defecto
export default Testimonio;
